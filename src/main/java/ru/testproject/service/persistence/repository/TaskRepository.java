package ru.testproject.service.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.testproject.service.persistence.domain.Task;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Repository
public interface TaskRepository extends JpaRepository<Task, UUID> {

    List<Task> findAllByStatusAndUpdateDateLessThan(Task.Status status, LocalDateTime expirationDate);

    List<Task> findAllByStatus(Task.Status status);
}
