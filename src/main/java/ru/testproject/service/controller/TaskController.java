package ru.testproject.service.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.testproject.service.job.running.TaskRunner;
import ru.testproject.service.dto.TaskStatusDto;
import ru.testproject.service.exception.DataNotFoundException;
import ru.testproject.service.exception.UuidFormatException;
import ru.testproject.service.persistence.domain.Task;
import ru.testproject.service.service.TaskService;

import java.util.UUID;

@RestController
@RequestMapping(path = "/task", produces = MediaType.APPLICATION_JSON_VALUE)
public class TaskController {

    private final TaskService taskService;
    private final TaskRunner taskRunner;

    public TaskController(TaskService taskService, TaskRunner taskRunner) {
        this.taskService = taskService;
        this.taskRunner = taskRunner;
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public UUID createTask() {
        Task task = taskService.createTask();

        UUID uuid = task.getId();
        taskRunner.putTask(uuid);

        return uuid;
    }

    @GetMapping(value = "/{id}")
    public TaskStatusDto findTask(@PathVariable String id) {
        Task task = taskService.find(id);
        return convertToDto(task);
    }

    private static TaskStatusDto convertToDto(Task task) {
        TaskStatusDto status = new TaskStatusDto();
        status.setStatus(task.getStatus().name());
        status.setLocalDateTime(task.getUpdateDate());
        return status;
    }

    @ExceptionHandler(value = DataNotFoundException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public String handleDataNotFoundException(DataNotFoundException exception) {
        return exception.getMessage();
    }

    @ExceptionHandler(value = UuidFormatException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public String handleUuidFormatException(UuidFormatException exception) {
        return exception.getMessage();
    }
}
