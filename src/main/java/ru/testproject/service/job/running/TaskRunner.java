package ru.testproject.service.job.running;

import java.util.UUID;

public interface TaskRunner {

    void putTask(UUID uuid);
}
