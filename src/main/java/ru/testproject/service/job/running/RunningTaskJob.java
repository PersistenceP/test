package ru.testproject.service.job.running;

public interface RunningTaskJob {

    void runningTask() throws InterruptedException;
}
