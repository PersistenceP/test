package ru.testproject.service.job.running;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import ru.testproject.service.service.TaskService;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

@Component
public class RunningTaskJobImpl implements TaskRunner, RunningTaskJob {

    private final LinkedBlockingQueue<UUID> createdTaskQueue = new LinkedBlockingQueue<>();
    private final TaskService taskService;

    public RunningTaskJobImpl(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostConstruct
    void init() {
        List<UUID> tasks = taskService.findAllCreatedTask();
        createdTaskQueue.addAll(tasks);
    }

    @Scheduled(fixedDelayString = "1000")
    public void runningTask() throws InterruptedException {
        UUID uuid = createdTaskQueue.take();
        taskService.startTask(uuid);
    }

    @Override
    public void putTask(UUID uuid) {
        Assert.notNull(uuid, "uuid is null");
        createdTaskQueue.add(uuid);
    }
}
