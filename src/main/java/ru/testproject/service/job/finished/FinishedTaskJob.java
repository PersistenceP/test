package ru.testproject.service.job.finished;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.testproject.service.persistence.domain.Task;
import ru.testproject.service.persistence.repository.TaskRepository;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class FinishedTaskJob {

    private static final int TASK_PROCESSING_TIME = 2;
    private final TaskRepository taskRepository;

    public FinishedTaskJob(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Scheduled(fixedDelayString = "1000")
    public void terminateTask() {
        List<Task> tasks = taskRepository.findAllByStatusAndUpdateDateLessThan(Task.Status.RUNNING,
                LocalDateTime.now().minusMinutes(TASK_PROCESSING_TIME));

        if (tasks.isEmpty()) {
            return;
        }

        tasks.forEach(task -> {
            task.setStatus(Task.Status.FINISHED);
            task.setUpdateDate(LocalDateTime.now());
        });
        taskRepository.saveAll(tasks);
    }
}
