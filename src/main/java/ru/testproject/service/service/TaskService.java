package ru.testproject.service.service;

import ru.testproject.service.persistence.domain.Task;

import java.util.List;
import java.util.UUID;

public interface TaskService {

    Task createTask();

    Task find(String id);

    void startTask(UUID uuid);

    List<UUID> findAllCreatedTask();
}
