package ru.testproject.service.service;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ru.testproject.service.exception.DataNotFoundException;
import ru.testproject.service.exception.UuidFormatException;
import ru.testproject.service.persistence.domain.Task;
import ru.testproject.service.persistence.repository.TaskRepository;
import ru.testproject.service.util.UuidUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task createTask() {
        Task task = new Task();
        task.setStatus(Task.Status.CREATED);
        task.setUpdateDate(LocalDateTime.now());

        taskRepository.saveAndFlush(task);
        return task;
    }

    @Override
    public Task find(String id) {
        if (UuidUtils.isNotValidUuid(id)) {
            throw new UuidFormatException(id);
        }

        return taskRepository.findById(UUID.fromString(id))
                .orElseThrow(() -> new DataNotFoundException(id));
    }

    @Override
    public void startTask(UUID uuid) {
        Assert.notNull(uuid, "uuid is null");

        Task task = taskRepository.findById(uuid)
                .orElseThrow(() -> new DataNotFoundException(uuid.toString()));

        task.setStatus(Task.Status.RUNNING);
        task.setUpdateDate(LocalDateTime.now());

        taskRepository.saveAndFlush(task);
    }

    @Override
    public List<UUID> findAllCreatedTask() {
        return taskRepository.findAllByStatus(Task.Status.CREATED).stream()
                .map(Task::getId)
                .collect(Collectors.toList());
    }
}
