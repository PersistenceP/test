package ru.testproject.service.exception;

public class DataNotFoundException extends RuntimeException {

    public DataNotFoundException(String id) {
        super("not found task with uuid: " + id);
    }
}
