package ru.testproject.service.exception;

public class UuidFormatException extends RuntimeException {

    public UuidFormatException(String message) {
        super("uuid not valid: " + message);
    }
}
