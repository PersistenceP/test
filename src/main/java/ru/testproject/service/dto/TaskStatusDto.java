package ru.testproject.service.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class TaskStatusDto implements Serializable {

    private String status;
    private LocalDateTime localDateTime;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }
}
