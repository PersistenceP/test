package ru.testproject.service.util;

public class UuidUtils {

    private static final String UUID_PATTERN = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34]" +
            "[0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}";


    private UuidUtils() {
    }

    public static boolean isNotValidUuid(String uuid) {
        return !validateUuid(uuid);
    }

    static boolean validateUuid(String uuid) {
        return uuid != null && uuid.matches(UUID_PATTERN);
    }
}
