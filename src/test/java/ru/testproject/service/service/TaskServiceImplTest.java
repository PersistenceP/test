package ru.testproject.service.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.testproject.service.exception.DataNotFoundException;
import ru.testproject.service.exception.UuidFormatException;
import ru.testproject.service.persistence.domain.Task;
import ru.testproject.service.persistence.repository.TaskRepository;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TaskServiceImplTest {

    @Mock
    private TaskRepository taskRepository;
    @InjectMocks
    private TaskServiceImpl taskService;

    @Test
    public void findTaskByUuid() {
        UUID uuid = UUID.randomUUID();
        Task task = new Task();
        when(taskRepository.findById(uuid)).thenReturn(Optional.of(task));

        Task resultTask = taskService.find(uuid.toString());

        assertThat(resultTask, is(task));
        verify(taskRepository).findById(uuid);
    }

    @Test
    public void findTaskByUuidWithNotValidUuid() {
        String uuid = "UUID";
        try {
            taskService.find(uuid);
            fail();
        } catch (UuidFormatException ex) {
            assertThat(ex.getMessage(), is("uuid not valid: " + uuid));
        }
    }

    @Test
    public void findNotExistingTaskByUuid() {
        UUID uuid = UUID.randomUUID();

        try {
            taskService.find(uuid.toString());
            fail();
        } catch (DataNotFoundException ex) {
            assertThat(ex.getMessage(), is("not found task with uuid: " + uuid));
        }

        verify(taskRepository).findById(uuid);
    }

    @Test
    public void createTask() {
        LocalDateTime now = LocalDateTime.now().minusSeconds(1);

        Task task = taskService.createTask();

        assertThat(task.getStatus(), is(Task.Status.CREATED));
        assertTrue(now.isBefore(task.getUpdateDate()));

        verify(taskRepository).saveAndFlush(task);
    }

    @Test
    public void startTask() {
        UUID uuid = UUID.randomUUID();
        LocalDateTime now = LocalDateTime.now().minusSeconds(1);

        Task task = new Task();
        when(taskRepository.findById(uuid)).thenReturn(Optional.of(task));

        taskService.startTask(uuid);

        assertThat(task.getStatus(), is(Task.Status.RUNNING));
        assertTrue(now.isBefore(task.getUpdateDate()));

        verify(taskRepository).findById(uuid);
        verify(taskRepository).saveAndFlush(task);
    }

    @Test
    public void startTaskByNullUuid() {
        try {
            taskService.startTask(null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage(), is("uuid is null"));
        }
    }

    @Test
    public void startNotExistingTask() {
        UUID uuid = UUID.randomUUID();
        when(taskRepository.findById(uuid)).thenReturn(Optional.empty());

        try {
            taskService.startTask(uuid);
            fail();
        } catch (DataNotFoundException ex) {
            assertThat(ex.getMessage(), is("not found task with uuid: " + uuid));
        }

        verify(taskRepository).findById(uuid);
    }

    @Test
    public void findAllCreatedTask() {
        Task task = new Task();
        task.setId(UUID.randomUUID());

        List<Task> tasks = Collections.singletonList(task);
        when(taskRepository.findAllByStatus(Task.Status.CREATED)).thenReturn(tasks);

        List<UUID> createdTasks = taskService.findAllCreatedTask();

        List<UUID> uuids = tasks.stream().map(Task::getId).collect(Collectors.toList());
        assertThat(createdTasks, is(uuids));
    }


    @Before
    public void verifyNoMore() {
        verifyNoMoreInteractions(taskRepository);
    }
}