package ru.testproject.service.controller;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.testproject.service.job.running.TaskRunner;
import ru.testproject.service.persistence.domain.Task;
import ru.testproject.service.service.TaskService;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(MockitoJUnitRunner.class)
public class TaskControllerTest {

    @Mock
    private TaskService taskService;
    @Mock
    private TaskRunner taskRunner;
    @InjectMocks
    private TaskController taskController;

    private MockMvc mockMvc;

    @Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders
                .standaloneSetup(taskController)
                .build();
    }

    @Test
    public void createTask() throws Exception {
        Task task = new Task();
        task.setId(UUID.randomUUID());

        when(taskService.createTask()).thenReturn(task);

        Gson gson = new Gson();
        mockMvc.perform(post("/task")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(content().string(gson.toJson(task.getId().toString())));

        verify(taskService).createTask();
        verify(taskRunner).putTask(task.getId());
    }

    @Test
    public void findTask() throws Exception {
        Task task = new Task();
        task.setId(UUID.randomUUID());
        task.setStatus(Task.Status.RUNNING);
        task.setUpdateDate(LocalDateTime.now());

        when(taskService.find(task.getId().toString())).thenReturn(task);

        mockMvc.perform(get("/task/{id}", task.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(task.getStatus().toString())));

        verify(taskService).find(task.getId().toString());
    }

    @Before
    public void verifyNoMore() {
        verifyNoMoreInteractions(taskService);
        verifyNoMoreInteractions(taskRunner);
    }
}