package ru.testproject.service.util;

import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

public class UuidUtilsTest {

    @Test
    public void validateUuid() {
        String uuid = UUID.randomUUID().toString();
        assertTrue(uuid, UuidUtils.validateUuid(uuid));
    }

    @Test
    public void validateNotValidUuid() {
        String uuid = "uuid";
        assertFalse(uuid, UuidUtils.validateUuid(uuid));
    }

    @Test
    public void validateNull() {
        assertFalse(UuidUtils.validateUuid(null));
    }
}