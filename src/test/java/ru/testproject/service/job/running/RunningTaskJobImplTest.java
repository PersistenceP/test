package ru.testproject.service.job.running;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import ru.testproject.service.service.TaskService;

import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class RunningTaskJobImplTest {

    @Mock
    private TaskService taskService;
    @InjectMocks
    private RunningTaskJobImpl runningTaskJob;

    @Test
    public void runningTask() throws InterruptedException {
        UUID uuid = UUID.randomUUID();
        runningTaskJob.putTask(uuid);

        runningTaskJob.runningTask();
        Mockito.verify(taskService).startTask(uuid);
    }

    @Test
    public void putNullUuid() {
        try {
            runningTaskJob.putTask(null);
            fail();
        } catch (Exception ex) {
            assertThat(ex.getMessage(), is("uuid is null"));
        }
    }

    @Before
    public void verifyNoMore() {
        verifyNoMoreInteractions(taskService);
    }
}