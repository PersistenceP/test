package ru.testproject.service.job.finished;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.testproject.service.persistence.domain.Task;
import ru.testproject.service.persistence.repository.TaskRepository;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FinishedTaskJobTest {

    @Mock
    private TaskRepository taskRepository;
    @InjectMocks
    private FinishedTaskJob finishedTaskJob;

    @Test
    public void terminateTask() {
        LocalDateTime now = LocalDateTime.now().minusSeconds(1);

        List<Task> tasks = asList(new Task(), new Task());
        when(taskRepository.findAllByStatusAndUpdateDateLessThan(eq(Task.Status.RUNNING), any(LocalDateTime.class)))
                .thenReturn(tasks);

        finishedTaskJob.terminateTask();

        for (Task task : tasks) {
            assertThat(task.getStatus(), is(Task.Status.FINISHED));
            assertTrue(now.isBefore(task.getUpdateDate()));
        }

        verify(taskRepository).findAllByStatusAndUpdateDateLessThan(eq(Task.Status.RUNNING), any(LocalDateTime.class));
        verify(taskRepository).saveAll(tasks);
    }

    @Test
    public void terminateEmptyListTask() {
        when(taskRepository.findAllByStatusAndUpdateDateLessThan(eq(Task.Status.RUNNING), any(LocalDateTime.class)))
                .thenReturn(Collections.emptyList());

        finishedTaskJob.terminateTask();

        verify(taskRepository).findAllByStatusAndUpdateDateLessThan(eq(Task.Status.RUNNING), any(LocalDateTime.class));
    }

    @Before
    public void verifyNoMore() {
        verifyNoMoreInteractions(taskRepository);
    }
}